FROM ubuntu:19.04

ENV LANG=en_US.UTF-8 \
    LANGUAGE=en_US:en \
    LC_ALL=en_US.UTF-8 \
    DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
    apt-get update && \
    apt-get install --yes \
    	    gcc \
	    g++ \
    	    cmake \
	    ninja-build \
	    qbs \
	    qt5-default \
	    qtbase5-dev \
	    qttools5-dev \
	    qtdeclarative5-dev \
    	    qtbase5-private-dev \
	    qtdeclarative5-private-dev \
	    libqt5script5 \
	    libqt5serialport5-dev \
	    libqt5svg5-dev \
	    llvm-8-dev \
	    libclang-8-dev \
	    qbs-dev \
    	    clang-8 qtscript5-dev \
	    qtwebengine5-dev \
	    libdw-dev \
	    time \
	    dstat && \
    qbs-setup-toolchains --detect && qbs-setup-qt --detect
