#!/usr/bin/env bash

set -euo pipefail

JOBS_LIST=$(seq 64 -2 2)
LOG_FORMAT="%C,%D,%E,%F,%I,%K,%M,%O,%P,%R,%S,%U,%W,%X,%Z,%c,%e,%k,%p,%r,%s,%t,%w,%x"
LOG_FILE="$PWD/build-qtcreator-stats.csv"
DSTAT_FILE="$PWD/build-qtcreator-dstat.csv"
HERE=$PWD

function log()
{
    build_system=$1; shift;
    jobs=$1; shift;
    date
    echo "Building QtCreator with $build_system using $jobs jobs";    
    TIME=$build_system,$jobs,$LOG_FORMAT \time -a -o $LOG_FILE $* > /dev/null 2>&1
}

function build_with_qbs()
{
    jobs=$1; shift
    build_dir=builds/qbs-$jobs
    [ -e $build_dir ] && rm -r $build_dir
    mkdir $build_dir
    pushd $build_dir > /dev/null
    log "Qbs" "$jobs" qbs -f $HERE/qtcreator.qbs -j $jobs profile:qt-5-12-2 project.withAutotests:false
    popd < /dev/null
    rm -r $build_dir
}

function build_with_cmake()
{
    jobs=$1; shift
    build_dir=builds/cmake-$jobs
    [ -e $build_dir ] && rm -r $build_dir
    mkdir $build_dir
    pushd $build_dir > /dev/null
    cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_CXX_FLAGS_DEBUG="-O0 -g" -G Ninja $HERE  > /dev/null 2>&1
    log "CMake/Ninja" "$jobs" ninja -j $jobs
    popd > /dev/null
    rm -r $build_dir 
}

[ -e $DSTAT_FILE ] && rm $DSTAT_FILE
dstat --epoch --disk --load --cpu --output $DSTAT_FILE >/dev/null 2>&1 &
dstat_pid=$!
[ -e $LOG_FILE ] && rm $LOG_FILE
for jobs in $JOBS_LIST;
do
    build_with_qbs $jobs
    build_with_cmake $jobs
done
kill $dstat_pid
